<div class="modal fade" id="confAgenda" tabindex="-1" role="dialog" aria-labelledby="confAgendaabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confAgendaabel">Agenda</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="agendaItems" class="content scroll">
                    <ul class="modal-tabs nav nav-tabs" role="tablist">
                        <li id="day1" class="active">
                            <a href="#">Auditorium 1</a>
                        </li>
                        <li id="day2" class="">
                            <a href="#">Auditorium 2</a>
                        </li>
                        <li id="day3" class="">
                            <a href="#">Auditorium 3</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="day1Agenda" style="display:block;">

                        </div>
                        <div class="tab-pane" id="day2Agenda" style="display:none;">

                        </div>
                        <div class="tab-pane" id="day3Agenda" style="display:none;">

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>