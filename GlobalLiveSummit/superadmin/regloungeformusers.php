<?php
require_once "../functions.php";
$title = 'GlobalLiveSummit';

$member = new Form();
$list = $member->getAllMemberList();
//var_dump($list);

$i = 0;
$data = array();
$ev = new Event();

foreach ($list as $user) {
  $country = '';
  $state = '';
  $city = '';
  // if ($user['country'] != '0') {
  //   $country =  $ev->getCountry($user['country']);
  // }
  // if ($user['state'] != '0') {
  //   $state =  $ev->getState($user['state']);
  // }
  // if ($user['city'] != '0') {
  //   $city =  $ev->getCity($user['city']);
  // }

  $data[$i]['Name'] = $user['name'] ;
  $data[$i]['speciality'] = $user['speciality'];
  $data[$i]['designation'] = $user['designation'];
  $data[$i]['address'] = $user['address'];
  $data[$i]['state'] = $user['state'];
  $data[$i]['city'] = $user['city'];
  // $data[$i]['Topics of Interest'] = $user['topic_interest'];
  $data[$i]['mobile'] = $user['phone'];
 // $data[$i]['Time of Registration'] = $user['reg_date'];
  $data[$i]['Last Login'] = $user['time1'];
  $data[$i]['Last Logout'] = $user['time'];

  $i++;
}
$filename = $title . "_users.xls";
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=\"$filename\"");
ExportFile($data);
