<?php
require_once 'header.php';
?>
<table class="table table-bordered">
    <thead>

        <tr>
            <th>Date</th>
            <th>Track</th>
            <th>ZOOM Link </th>
        </tr>
    </thead>

    <tr>
        <td>19 Feb </td>
        <td>1. Common Session </td>
        <td><a href="https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09" target="_blank">https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09</a></td>
    </tr>
    <tr>
        <td>19 Feb </td>
        <td>2. Integrace Health Audi 1 </td>
        <td><a href="https://us06web.zoom.us/j/82175639241?pwd=aXU3ajhlRmtYVzlORms3czh4dENtUT09" target="_blank">https://us06web.zoom.us/j/82175639241?pwd=aXU3ajhlRmtYVzlORms3czh4dENtUT09</a></td>
    </tr>
    <tr>
        <td>19 Feb </td>
        <td>3. Integrace Health Audi 2 </td>
        <td><a href="https://zoom.us/j/97879016179?pwd=NmNyZ2o1UVhqL0dLWmtPRVdkMHVJQT09" target="_blank">https://zoom.us/j/97879016179?pwd=NmNyZ2o1UVhqL0dLWmtPRVdkMHVJQT09</a></td>
    </tr>
    <tr>
        <td>19 Feb </td>
        <td>4. Integrace Health Audi 3 </td>
        <td><a href="https://zoom.us/j/92415148081?pwd=WStGNG9adnJtdzhiTnBZKzUzcmtWUT09" target="_blank">https://zoom.us/j/92415148081?pwd=WStGNG9adnJtdzhiTnBZKzUzcmtWUT09</a></td>
    </tr>
   
    <!-- <tr class="bg-light">
        <td colspan="3"></td>
    </tr> -->
    <!-- <tr>
        <td>19 FEb 05:05 PM  </td>
        <td>4. Innovation: Building A Creative Mindset (Auditorium 1) </td>
        <td><a href="https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09" target="_blank">https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09</a></td>
    </tr>
    <tr>
        <td>19 FEb 05:05 PM </td>
        <td>5. Innovation: Building A Creative Mindset (Auditorium 2) </td>
        <td><a href="https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09" target="_blank">https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09</a></td>
    </tr>
    <tr>
    <td>19 FEb 05:05 PM </td>
        <td>6. Innovation: Building A Creative Mindset (Auditorium 3) </td>
        <td><a href="https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09" target="_blank">https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09</a></td>
    </tr>
   
    <tr class="bg-light">
        <td colspan="3"></td>
    </tr>
    <tr>
        <td>19 FEb 05:20 PM  </td>
        <td>7. Hip Arthroplasty (Auditorium 1) </td>
        <td><a href="https://us06web.zoom.us/j/82175639241?pwd=aXU3ajhlRmtYVzlORms3czh4dENtUT09" target="_blank">https://us06web.zoom.us/j/82175639241?pwd=aXU3ajhlRmtYVzlORms3czh4dENtUT09</a></td>
    </tr>
    <tr>
        <td>19 FEb 05:20 PM </td>
        <td>8. Oncology (Auditorium 2) </td>
        <td><a href="https://zoom.us/j/97879016179?pwd=NmNyZ2o1UVhqL0dLWmtPRVdkMHVJQT09" target="_blank">https://zoom.us/j/97879016179?pwd=NmNyZ2o1UVhqL0dLWmtPRVdkMHVJQT09</a></td>
    </tr>
    <tr>
    <td>19 FEb 05:20 PM </td>
        <td>9. Arthroscopy (Auditorium 3) </td>
        <td><a href="https://zoom.us/j/92415148081?pwd=WStGNG9adnJtdzhiTnBZKzUzcmtWUT09" target="_blank">https://zoom.us/j/92415148081?pwd=WStGNG9adnJtdzhiTnBZKzUzcmtWUT09</a></td>
    </tr>

    <tr class="bg-light">
        <td colspan="3"></td>
    </tr>
    <tr>
        <td>19 FEb 06:35 PM  </td>
        <td>10. Knee Arthroplasty (Auditorium 1) </td>
        <td><a href="https://us06web.zoom.us/j/82175639241?pwd=aXU3ajhlRmtYVzlORms3czh4dENtUT09" target="_blank">https://us06web.zoom.us/j/82175639241?pwd=aXU3ajhlRmtYVzlORms3czh4dENtUT09</a></td>
    </tr>
    <tr>
        <td>19 FEb 06:35 PM </td>
        <td>11. spine (Auditorium 2) </td>
        <td><a href="https://zoom.us/j/97879016179?pwd=NmNyZ2o1UVhqL0dLWmtPRVdkMHVJQT09" target="_blank">https://zoom.us/j/97879016179?pwd=NmNyZ2o1UVhqL0dLWmtPRVdkMHVJQT09</a></td>
    </tr>
    <tr>
    <td>19 FEb 06:35 PM </td>
        <td>12. Trauma (Auditorium 3) </td>
        <td><a href="https://zoom.us/j/92415148081?pwd=WStGNG9adnJtdzhiTnBZKzUzcmtWUT09" target="_blank">https://zoom.us/j/92415148081?pwd=WStGNG9adnJtdzhiTnBZKzUzcmtWUT09</a></td>
    </tr>

    <tr class="bg-light">
        <td colspan="3"></td>
    </tr>
    <tr>
        <td>19 FEb 07:50 PM  </td>
        <td>13.Global Changes of Hospital Acquired Infection & Anti Microbial Resistance (Auditorium 1) </td>
        <td><a href="https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09" target="_blank">https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09</a></td>
    </tr>
    <tr>
        <td>19 FEb 07:50 PM </td>
        <td>14. Global Changes of Hospital Acquired Infection & Anti Microbial Resistance (Auditorium 2) </td>
        <td><a href="https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09" target="_blank">https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09</a></td>
    </tr>
    <tr>
    <td>19 FEb 07:50 PM </td>
        <td>15. Global Changes of Hospital Acquired Infection & Anti Microbial Resistance (Auditorium 3) </td>
        <td><a href="https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09" target="_blank">https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09</a></td>
    </tr>

    <tr class="bg-light">
        <td colspan="3"></td>
    </tr>
    <tr>
        <td>19 FEb 08:05 PM  </td>
        <td>16. Valedictory and Vote of thanks (Auditorium 1) </td>
        <td><a href="https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09" target="_blank">https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09</a></td>
    </tr>
    <tr>
        <td>19 FEb 08:05 PM </td>
        <td>17. Valedictory and Vote of thanks (Auditorium 2) </td>
        <td><a href="https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09" target="_blank">https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09</a></td>
    </tr>
    <tr>
    <td>19 FEb 08:05 PM </td>
        <td>18. Valedictory and Vote of thanks (Auditorium 3) </td>
        <td><a href="https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09" target="_blank">https://zoom.us/j/99808584201?pwd=cDU5ZnRpRW1OMTJqemNDWkJpeHh2UT09</a></td>
    </tr> -->
   
</table>
<?php
require_once 'footer.php';
?>