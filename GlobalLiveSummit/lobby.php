<?php
require_once "logincheck.php";
$curr_room = 'lobby';
$audi_id1 = '9542018936806c44c9e70be0bc37ba07a129d4b0e3783ef07acbb7a839381514';
$audi_id2 = '9f7cd6782b7cf4716771e9c7d5328c95549b3bda9833cde8f3e7449ed59c8e08';
$audi_id3 = 'd92d15f91a4b105183d3665531c7fc7a67c98176e22bcc96c3dc91aee56c6334';

?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/images/zoomed in LOBBY.jpg">
            <!-- <div id="lobbyVideo">
                <iframe src="https://player.vimeo.com/video/543708869?autoplay=1&loop=1" width="100%" height="100%" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
            </div> -->

            <!-- <a href="#" id="enterAudi1" onclick="enterAudi1()">
                <div class="indicator d-6"></div>
            </a>
            <a href="#" id="enterAudi2" onclick="enterAudi2()">
                <div class="indicator d-6"></div>
            </a>
            <a href="#" id="enterAudi3" onclick="enterAudi3()">
                <div class="indicator d-6"></div>
            </a> -->
            
            <a href="auditorium1.php" id="enterAudi1" >
                <div class="indicator d-7"></div>
            </a>
            <a href="auditorium2.php" id="enterAudi2" >
                <div class="indicator d-7"></div>
            </a>
            <a href="auditorium3.php" id="enterAudi3" >
                <div class="indicator d-7"></div>
            </a>
            <!-- <a href="games.php" id="engagement">
                <div class="indicator d-6"></div>
            </a> -->
            <a href="exhibitionhalls.php" id="enterHall">
                <div class="indicator d-7"></div>
            </a>
            <a href="lounge.php" id="enterLounge">
                <div class="indicator d-7"></div>
            </a>
            <a href="photobooth.php" id="photobooth">
                <div class="indicator d-7"></div>
            </a>
            <!-- <a href="timeline.php" id="timeline">
                <div class="indicator d-6"></div>
            </a> -->
            <!-- <a href="assets/resources/conf-agenda.pdf" class="showpdf" id="showAgenda"> -->
            <a href="#"  id="showAgenda">
                <div class="indicator d-4"></div>
            </a>
            <a href="assets/resources/11821 Global Live Summit Faculty Profile.PDF" class="showpdf" id="timeline" onclick=knowyourexpert()>
                <div class="indicator d-4"></div>
            </a>
             <!-- <a href="#" onclick="javascript:alert('Certificate will be available for download on 9th May 2021');" id="digitalcertificate">  -->
            <a href="digital_cert.php" id="digitalcertificate">
                <div class="indicator d-4"></div>
            </a>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
    <?php require_once "scripts.php" ?>
    <?php require_once "audi-common-lobby.php"?>
    <?php require_once "audi-script-lobby.php" ?>
</div>
<section class="videotoplay" id="gotoaudi1" style="display:none;">
    <video class="videoplayer" id="gotoaudi1video" preload="auto">
        <source src="toaudi.mp4" type="video/mp4">
    </video>
    <a href="auditorium1.php" class="skip">SKIP</a>
</section>
<section class="videotoplay" id="gotoaudi2" style="display:none;">
    <video class="videoplayer" id="gotoaudi2video" preload="auto">
        <source src="toaudi.mp4" type="video/mp4">
    </video>
    <a href="auditorium2.php" class="skip">SKIP</a>
</section>
<section class="videotoplay" id="gotoaudi3" style="display:none;">
    <video class="videoplayer" id="gotoaudi3video" preload="auto">
        <source src="toaudi.mp4" type="video/mp4">
    </video>
    <a href="auditorium3.php" class="skip">SKIP</a>
</section>
<?php require_once "scripts.php" ?>
<script>
    var audi1Video = document.getElementById("gotoaudi1video");
    audi1Video.addEventListener('ended', audi1End, false);
    var audi2Video = document.getElementById("gotoaudi2video");
    audi2Video.addEventListener('ended', audi2End, false);
    var audi3Video = document.getElementById("gotoaudi3video");
    audi3Video.addEventListener('ended', audi3End, false);

    function knowyourexpert(){
     console.log("inside Digital Certificate");
     var vid_id = $(this).data('vidid');
      $.ajax({
        url: 'control/exhib.php',
        data: {
          action: 'updateVideoView',
          vidId: 2,
          userId: '<?php echo $_SESSION['userid']; ?>',
        },
        type: 'post',
        success: function(response) {
          //console.log(response);
        }
      });
    }

    function enterAudi1() {
        $('#content').css('display', 'none');
        $('#gotoaudi1').css('display', 'block');
        audi1Video.currentTime = 0;
        audi1Video.play();
    }

    function audi1End(e) {
        $('#gotoaudi1').fadeOut(500);
        setTimeout(function() {
            location.href = "auditorium1.php";
        }, 1000);

    }

    function enterAudi2() {
        $('#content').css('display', 'none');
        $('#gotoaudi2').css('display', 'block');
        audi2Video.currentTime = 0;
        audi2Video.play();
    }

    function audi2End(e) {
        $('#gotoaudi2').fadeOut(500);
        setTimeout(function() {
            location.href = "auditorium2.php";
        }, 1000);
    }

    function enterAudi3() {
        $('#content').css('display', 'none');
        $('#gotoaudi3').css('display', 'block');
        audi3Video.currentTime = 0;
        audi3Video.play();
    }

    function audi3End(e) {
        $('#gotoaudi3').fadeOut(500);
        setTimeout(function() {
            location.href = "auditorium3.php";
        }, 1000);
    }

    $(function() {
        /* $.magnificPopup.open({
            items: {
                src: 'https://player.vimeo.com/video/543708869'
            },
            type: 'iframe',
            iframe: {
                markup: '<div class="mfp-iframe-scaler">' +
                    '<div class="mfp-close"></div>' +
                    '<iframe class="mfp-iframe" frameborder="0" allow="autoplay; fullscreen" allowfullscreen ></iframe>' +
                    '<div class="mfp-title"></div>' +
                    '</div>'
            },
            removalDelay: 300,
            mainClass: 'mfp-fade'
        }); */
        $(document).on('click', '#resource', function() {
            //alert();
            $('#resourcesList').modal('show');
        });
    });
</script>

<div class="modal fade" id="resourcesList" tabindex="-1" role="dialog" aria-labelledby="resourcesListTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="resourcesListLongTitle">Resources</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="content scroll">

                    <ul class="popuplist">
                        <li>
                            <i class="far fa-file-pdf"></i> <a href="assets/resources/conf/profiles.pdf" class="viewpoppdf resdl">International Faculty Profiles</a>
                        </li>
                        <li>
                            <i class="far fa-file-pdf"></i> <a href="assets/resources/conf/track1.pdf" class="viewpoppdf resdl">Track-1 UPPER EXTREMITY TRAUMA</a>
                        </li>
                        <li>
                            <i class="far fa-file-pdf"></i> <a href="assets/resources/conf/track2.pdf" class="viewpoppdf resdl">Track-2 FOOT ANKLE TRAUMA </a>
                        </li>
                        <li>
                            <i class="far fa-file-pdf"></i> <a href="assets/resources/conf/track3a.pdf" class="viewpoppdf resdl">Track-3 PATHOLOGICAL FRACTURES</a>
                        </li>
                        <li>
                            <i class="far fa-file-pdf"></i> <a href="assets/resources/conf/track3b.pdf" class="viewpoppdf resdl">Track-3 SPINE TRAUMA</a>
                        </li>
                        <li>
                            <i class="far fa-file-pdf"></i> <a href="assets/resources/conf/track4.pdf" class="viewpoppdf resdl">Track-4 KNEE TRAUMA</a>
                        </li>
                        <li>
                            <i class="far fa-file-pdf"></i> <a href="assets/resources/conf/track5.pdf" class="viewpoppdf resdl">Track-5 HIP PELVIS TRAUMA</a>
                        </li>
                        <li>
                            <i class="far fa-file-pdf"></i> <a href="assets/resources/conf/track6.pdf" class="viewpoppdf resdl">Track-6 INFECTION MANAGEMENT TRAUMA</a>
                        </li>
                    </ul>

                </div>
            </div>

        </div>
    </div>
</div>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>
