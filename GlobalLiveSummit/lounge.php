<?php
require_once "logincheck.php";
$curr_room = 'lounge';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg" class="pt-4">
            <img src="assets/images/AIIMS Networking Lounge.jpg">
            <a class="show_attendees" id="shareCards" href="#">
                <div class="indicator d-6"></div>
            </a>
            <a class="show_attendees" id="chatAttendees" href="#">
                <div class="indicator d-6"></div>
            </a>
            <!-- <a class="" id="chatAttendees1" data-toggle="modal" data-target="#exampleModal" href="#">
                <div class="indicator d-6"></div>
            </a> -->
            <a class="" id="chatAttendees1" href="#"  data-toggle="modal" data-target="#add_data_Modal">
                <div class="indicator d-6"></div>
              
            </a>
          
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
    <div class="modal fade" id="attendees" tabindex="-1" role="dialog" aria-labelledby="attendeesLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="attendeesLabel">Attendees</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-1">
                    <ul class="modal-tabs nav nav-tabs" role="tablist">
                        <li id="listAttendees" class="active">
                            <a href="#">Attendees</a>
                        </li>
                        <li id="chatInbox" class="">
                            <a href="#">Chat Inbox</a>
                        </li>
                        <li id="cardsShared" class="">
                            <a href="#">Shared Cards</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="attendeesList" style="display:block;">
                            <div class="search-area">
                                <form method="post" >
                                    <div class="row">
                                        <div class="col-12 col-md-6 pr-0">
                                            <input type="text" id="attendee-search" placeholder="Search by name" class="input">
                                        </div>
                                        <div class="col-12 col-md-4 text-left">
                                            <button type="submit" id="search-attendee" value="Search">Search</button><button type="submit" id="clear-search-attendee" value="Clear">Clear</button>
                                        </div>
                                        <div class="col-12 col-md-2 text-right">
                                            <button type="button" id="refresh-attendees">Refresh</button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                            <div id="attendeeList" class="content scroll"></div>
                        </div>
                        <div class="tab-pane" id="inboxChat" style="display:none;">
                            <div id="attendees-list-chat" class="content scroll">

                            </div>
                        </div>
                        <div class="tab-pane" id="sharedCards" style="display:none;">
                            <div id="cards-shared" class="content scroll">

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
</button> -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Contact Us</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p class="text-dark text-center mt-4">
          For Further Assistance:  <a href="mailto:contactus@integracehealth.com" class="text-dark">contactus@integracehealth.com</a>
          </p>
      
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>



<div id="add_data_Modal" class="modal fade p-3">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header"> <br>
   <h5 class="modal-title text-light">Drop your details for Integrace employee to connect with you</h5>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
   
    
   </div>
   <div class="modal-body">
   <form method="post" id="insert_form" class="p-4" action="formsubmit.php">
       <div class="container">
           <div class="row">
               <div class="col-sm-12">
               <label>Name</label>
                     <input type="text" name="name" id="name" class="form-control" required />
               </div>     
           </div>
           <div class="row">
               <div class="col-sm-6">
               <label>Speciality</label>
                    <input type="text" name="speciality" id="speciality" class="form-control" required />
               </div>
               <div class="col-sm-6">
               <label>Hospital/clinic name</label>
                <input type="text" name="designation" id="designation" class="form-control" required />
               </div>
           </div>
           <div class="row">
               <div class="col-sm-12">
               <label>Complete address</label>
                <textarea name="address" id="address" class="form-control" required></textarea>
               </div>
           </div>
           <div class="row">
               <div class="col-sm-6">
               <label>City</label>
                 <input type="text" name="city" id="city" class="form-control" required />
               </div>
               <div class="col-sm-6">
               <label>State</label> 
                <input type="text" name="state" id="state" class="form-control" required />
               </div>
           </div>
           <div class="row">
               <div class="col-sm-6">
               <label>Phone No.</label>
                    <input type="text" name="phone" id="phone" class="form-control" required />
               </div>
               <div class="col-sm-3">
               <label>In-time</label>
                    <input type="time" name="time1" id="time" class="form-control" required />
               </div>
               <div class="col-sm-3">
               <label>Out-time</label>
                    <input type="time" name="time" id="time" class="form-control" required />
               </div>
           </div>
       </div>
  
     
     
     <!-- <label>Select Gender</label>
     <select name="gender" id="gender" class="form-control">
      <option value="Male">Male</option>  
      <option value="Female">Female</option>
     </select>-->
     <br />   
     
     <input type="submit" name="insert" id="insert" value="Submit" class="btn btn-primary" />

    </form>
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
   </div>
  </div>
 </div>
</div>

<div id="dataModal" class="modal fade">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title">Employee Details</h4>
   </div>
   <div class="modal-body" id="employee_detail">
    
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
   </div>
  </div>
 </div>
</div>

<script>  
$(document).ready(function(){
 $('#insert_form').on("submit", function(event){  
  event.preventDefault();  
  if($('#name').val() == "")  
  {  
   alert("Name is required");  
  }  
  else if($('#address').val() == '')  
  {  
   alert("Address is required");  
  }  
  else if($('#designation').val() == '')
  {  
   alert("Designation is required");  
  }
   
  else  
  {  
   $.ajax({  
    url:"insert.php",  
    method:"POST",  
    data:$('#insert_form').serialize(),  
    beforeSend:function(){  
     $('#insert').val("Inserting");  
    },  
    success:function(data){  
     $('#insert_form')[0].reset();  
     $('#add_data_Modal').modal('hide');  
     $('#employee_table').html(data);  
    }  
   });  
  }  
 });


 $(document).on('click', '.view_data', function(){
  //$('#dataModal').modal();
  var employee_id = $(this).attr("id");
  $.ajax({
   url:"select.php",
   method:"POST",
   data:{employee_id:employee_id},
   success:function(data){
    $('#employee_detail').html(data);
    $('#dataModal').modal('show');
   }
  });
 });
});  
 </script>

<?php require_once "scripts.php" ?>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>