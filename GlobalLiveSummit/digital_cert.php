<?php
require_once "logincheck.php";
$curr_room = 'digital_cert';
$exhib_id = "9d4346f60173d5bde86261d559953efa1ffb6669f60f0bb069e65f5aae392f55";
// echo "hello test" ;
// exit;
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>

<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/bg.jpg">
            <div id="cert-area">
                <div class="cert">
                    <a href="#" onclick="dlCert()"><img class="photo-jacket" src="assets/img/certificate.jpg" /></a>
                    <div class="name-text"><?= $user_name ?></div>
                </div>
                <a href="#" id="dlCert" onclick="dlCert()">Download Certificate</a>
            </div>




        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>

<?php require_once "scripts.php" ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.2/FileSaver.js"></script>
<script type="text/javascript" src="assets/js/html2canvas.min.js"></script>
<script>
    function dlCert() {
        html2canvas(document.querySelector("#cert-area"), {
            backgroundColor: "#000000"
        }).then(c => {
            c.toBlob(function(blob) {
                saveAs(blob, "<?= $user_name ?>_certificate.jpg");
            });
        });
    }

    
  $(function() {
     console.log("inside Digital Certificate");
     var vid_id = $(this).data('vidid');
      $.ajax({
        url: 'control/exhib.php',
        data: {
          action: 'updateVideoView',
          vidId: 1,
          userId: '<?php echo $_SESSION['userid']; ?>',
        },
        type: 'post',
        success: function(response) {
          //console.log(response);
        }
      });

})

</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>