<script>
  var chkPollRes;

  $(function() {

    $(document).on('click', '#showaudiAgenda', function() {
      $('#confAgenda').modal('show');
    });

    $(document).on('click', '#showAgenda', function() {
      $('#confAgenda').modal('show');
    });

    $(document).on('click', '#day1', function() {
      $('#day2').removeClass('active');
      $('#day3').removeClass('active');
      $('#day1').addClass('active');
      $('#day1Agenda').css('display', 'block');
      $('#day2Agenda').css('display', 'none');
      $('#day3Agenda').css('display', 'none');
      getSessions('day1', '<?= $audi_id1; ?>', '#day1Agenda',1);
    });

    $(document).on('click', '#day2', function() {
      $('#day1').removeClass('active');
      $('#day3').removeClass('active');
      $('#day2').addClass('active');
      $('#day2Agenda').css('display', 'block');
      $('#day1Agenda').css('display', 'none');
      $('#day3Agenda').css('display', 'none');
      getSessions('day2', '<?= $audi_id2; ?>', '#day2Agenda',2);
    });
    $(document).on('click', '#day3', function() {
      $('#day1').removeClass('active');
      $('#day2').removeClass('active');
      $('#day3').addClass('active');
      $('#day3Agenda').css('display', 'block');
      $('#day1Agenda').css('display', 'none');
      $('#day2Agenda').css('display', 'none');
      getSessions('day3', '<?= $audi_id3; ?>', '#day3Agenda',3);
    });

    $('#confAgenda').on('show.bs.modal', function(e) {
      //alert();
      getAllSessions();
    })
    $('#confAgenda').on('hidden.bs.modal', function(e) {});

    $(document).on('click', '#askques', function() {
      $('.poll').removeClass('show');
      $('.ques').toggleClass('show');
    });

    $(document).on('click', '#close_ques', function() {
      $('.ques').toggleClass('show');
    });

    $(document).on('click', '#takepoll', function() {
      $('.ques').removeClass('show');
      $('.poll').toggleClass('show');
    });

    $(document).on('click', '#close_poll', function() {
      $('.poll').toggleClass('show');
    });

    $(document).on('click', '.send_sesques', function() {
      var sess_id = $(this).data('ses');
      var user_id = $(this).data('user');
      var ques = $('#userques').val();

      if (ques != '') {

        $.ajax({
          url: 'control/ques.php',
          data: {
            action: 'submitques',
            sessId: sess_id,
            userId: user_id,
            ques: ques
          },
          type: 'post',
          success: function(message) {
            console.log(message);
            var response = JSON.parse(message);
            var status = response['status'];
            var msg = response['message'];
            if (status == 'success') {
              $('#userques').val('');
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                text: msg,
                showConfirmButton: false,
                timer: 2000
              })

            } else {
              Swal.fire({
                position: 'top-end',
                icon: 'error',
                text: msg,
                showConfirmButton: false,
                timer: 2000
              })
            }

          }
        });
      } else {
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          text: 'Please enter your question',
          showConfirmButton: false,
          timer: 2000
        })

      }

    });

   


  });

 
  function getAllSessions() {
    getSessions('day1', '<?= $audi_id1 ?>', '#day1Agenda',1);
    getSessions('day2', '<?= $audi_id2 ?>', '#day2Agenda',2);
    getSessions('day3', '<?= $audi_id3 ?>', '#day3Agenda',3);
  }

  function getSessions(day, audi, elem, audino) {
    $.ajax({
      url: 'control/server.php',
      data: {
        action: 'getSessions',
        audiId: audi,
       // audiId: "9542018936806c44c9e70be0bc37ba07a129d4b0e3783ef07acbb7a839381514",
        //day: day,
        day:"day1",
        key:'',
        audino:audino
      },
      type: 'post',
      success: function(response) {
        console.log(response);
        $(elem).html(response);

      }
    });

  }

  
</script>
<script src="https://player.vimeo.com/api/player.js"></script>
<script>
  var iframe = document.querySelector('iframe');
  var player = new Vimeo.Player(iframe);
  $('#goFS').on('click', function() {
    player.requestFullscreen().then(function() {
      // the player entered fullscreen
    }).catch(function(error) {
      // an error occurred
      console.log(error);
    });

  });
</script>