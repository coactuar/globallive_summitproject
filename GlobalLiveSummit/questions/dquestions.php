<?php
require_once "../functions.php";
$title = 'GlobalLiveSummit';

$Ques = new Question();
$lists = $Ques->getAllQuestion();
//var_dump($list);
$i = 0;
$data = array();
$ev = new Event();

foreach ($lists as $list) {
  // $country = '';
  // $state = '';
  // $city = '';
  // if ($user['country'] != '0') {
  //   $country =  $ev->getCountry($user['country']);
  // }
  // if ($user['state'] != '0') {
  //   $state =  $ev->getState($user['state']);
  // }
  // if ($user['city'] != '0') {
  //   $city =  $ev->getCity($user['city']);
  // }

  // $data[$i]['id'] = $list['id'] ;
  // $data[$i]['quesid'] = $list['quesid'];
 
  $data[$i]['UserName'] =  $list['first_name'] . ' ' .  $list['last_name'];
  $data[$i]['Session Title'] = $list['session_title'];
  // $data[$i]['userid'] = $list['userid'] ;
  $data[$i]['Questions'] = $list['question'] ;
  
  $data[$i]['Time'] = $list['asked_at'] ;
  $i++;
}
$filename = $title . "_questions.xls";
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=\"$filename\"");
ExportFile($data);
