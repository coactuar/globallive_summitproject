<?php
//require_once "../inc/config.php";
require_once "../functions.php";

if (isset($_POST['action']) && !empty($_POST['action'])) {

    $action = $_POST['action'];

    switch ($action) {

        case 'getformfeedback':
            $page_no = $_POST['pagenum'];
            $offset = ($page_no - 1) * $total_records_per_page;
            $previous_page = $page_no - 1;
            $next_page = $page_no + 1;
            $member = new Form();
            $member->__set('limit', $total_records_per_page);
            $total_records = $member->getUserCount();
           // $total_online = $member->getOnlineMemberCount();
            $total_no_of_pages = ceil($total_records / $total_records_per_page);
            $second_last = $total_no_of_pages - 1; // total page minus 1
            $usersList = $member->getAllMemberList($offset);
            //var_dump($usersList);
            if (!empty($usersList)) {
            ?>
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <!-- <td scope="col"><b>Registered Users:</b> <?= $total_records ?></td>
                            <td scope="col"><b>Online Users:</b> <?= $total_online ?></td> -->
                            <td scope="col"><a href="regloungeformusers.php">Download Form</a></td>
                            <!-- <td scope="col"><a href="onlineusers.php">Download Online Users</a></td>
                            <td scope="col"><a href="uservisits.php">Download Logins Report</a></td> -->
                        </tr>
                </table>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <!-- <th scope="col" width="50"></th> -->
                            <th scope="col" width="150">Name</th>
                            <th scope="col" width="150">Speciality</th>
                            <th scope="col">Designation</th>
                            <th scope="col" width="150">address</th>
                          
                            <th scope="col">City</th>
                            <th scope="col">State</th>
                            <th scope="col">Mobile</th>
                            <th scope="col">Logout Time</th>
                            <th scope="col">Loin Time</th>
                            

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($usersList as $a) { ?>
                            <tr>
                                <!-- <td>
                                    <a href="#" onclick="delUser('<?= $a['userid'] ?>')"><i class="fas fa-trash-alt"></i></a>
                                </td> -->
                                <td><?= $a['name'] ?></td>
                                <td style="max-width: 200px;"><?= $a['speciality'] ?></td>
                                <td><?= $a['designation'] ?></td>
                                <td><?= $a['address'] ?></td>
                                <td><?= $a['city'] ?></td>
                                <td><?= $a['state'] ?></td>
                                <td><?= $a['phone'] ?></td>
                                <td><?= $a['time'] ?></td>
                                <td><?= $a['time1'] ?></td>
                                
                               

                                


                            </tr>
                        <?php } ?>
                    </tbody>

                </table>

                <ul class="pagination">
                    <?php // if($page_no > 1){ echo "<li><a href='?page_no=1'>First Page</a></li>"; } 
                    ?>

                    <li class='page-item <?php if ($page_no <= 1) {
                                                echo "disabled";
                                            } ?>'>
                        <a class='page-link' <?php if ($page_no > 1) {
                                                    echo "onClick='gotoPage($previous_page)' href='#'";
                                                } ?>>Previous</a>
                    </li>

                    <?php
                    if ($total_no_of_pages <= 10) {
                        for ($counter = 1; $counter <= $total_no_of_pages; $counter++) {
                            if ($counter == $page_no) {
                                echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                            } else {
                                echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                            }
                        }
                    } elseif ($total_no_of_pages > 10) {

                        if ($page_no <= 4) {
                            for ($counter = 1; $counter < 8; $counter++) {
                                if ($counter == $page_no) {
                                    echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                                } else {
                                    echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                                }
                            }
                            echo "<li class='page-item'><a>...</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage($second_last)' href='#'>$second_last</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage($total_no_of_pages)' href='#'>$total_no_of_pages</a></li>";
                        } elseif ($page_no > 4 && $page_no < $total_no_of_pages - 4) {
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage(1)' href='#'>1</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage(2)' href='#'>2</a></li>";
                            echo "<li class='page-item'><a>...</a></li>";
                            for ($counter = $page_no - $adjacents; $counter <= $page_no + $adjacents; $counter++) {
                                if ($counter == $page_no) {
                                    echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                                } else {
                                    echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                                }
                            }
                            echo "<li class='page-item'><a>...</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage($second_last)' href='#'>$second_last</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage($total_no_of_pages)' href='#'>$total_no_of_pages</a></li>";
                        } else {
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage(1)' href='#'>1</a></li>";
                            echo "<li class='page-item'><a class='page-link' onClick='gotoPage(2)' href='#'>2</a></li>";
                            echo "<li class='page-item'><a>...</a></li>";

                            for ($counter = $total_no_of_pages - 6; $counter <= $total_no_of_pages; $counter++) {
                                if ($counter == $page_no) {
                                    echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                                } else {
                                    echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                                }
                            }
                        }
                    }
                    ?>

                    <li class='page-item <?php if ($page_no >= $total_no_of_pages) {
                                                echo "disabled";
                                            } ?>'>
                        <a class='page-link' <?php if ($page_no < $total_no_of_pages) {
                                                    echo "onClick='gotoPage($next_page)' href='#'";
                                                } ?>>Next</a>
                    </li>
                    <?php if ($page_no < $total_no_of_pages) {
                        echo "<li class='page-item'><a class='page-link' onClick='gotoPage($total_no_of_pages)' href='#'>Last &rsaquo;&rsaquo;</a></li>";
                    } ?>
                </ul>
            <?php
            }

            break;

       

            break;
    }
}
